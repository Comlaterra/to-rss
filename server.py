"""
Simple Flask server that provides paths to the various RSS feeds.

"""

from os import path

from flask import abort, Flask, Response

from jinja2 import Environment, FileSystemLoader

from to_rss.nhl import nhl_news, team_news, VALID_TEAMS
from to_rss.patreon import patreon_posts
from to_rss.pottermore import pottermore_page
from to_rss.wikipedia import get_articles

app = Flask(__name__)

# Jinja2 environment.
root = path.dirname(path.abspath(__file__))
env = Environment(loader=FileSystemLoader(path.join(root, 'to_rss', 'templates')))


@app.route('/')
def serve_about():
    """A link to each endpoint that's supported."""
    template = env.get_template('index.html')
    return template.render()


# Wikipedia end points.
@app.route('/wikipedia/')
def serve_wikipedia():
    template = env.get_template('wikipedia.html')
    return template.render()


@app.route('/wikipedia/current_events/')
def serve_wikipedia_current_events():
    return Response(get_articles(), mimetype='application/rss+xml')


# NHL end points.
@app.route('/nhl/')
def serve_nhl():
    template = env.get_template('nhl.html')
    return template.render(teams=VALID_TEAMS)


@app.route('/nhl/news/')
def serve_nhl_news():
    return Response(nhl_news(), mimetype='application/rss+xml')


@app.route('/nhl/<team>/')
def serve_nhl_team_news(team):
    if team not in VALID_TEAMS:
        abort(404)

    return Response(team_news(team), mimetype='application/rss+xml')


# Patreon end points.
@app.route('/patreon/')
def serve_patreon():
    template = env.get_template('patreon.html')
    return template.render()


@app.route('/patreon/<user>/')
def serve_patreon_user(user):
    return Response(patreon_posts(user), mimetype='application/rss+xml')


# Pottermore endpoints.
@app.route('/pottermore/')
def serve_pottermore():
    template = env.get_template('pottermore.html')
    return template.render()


@app.route('/pottermore/news/')
def serve_pottermore_news():
    return Response(pottermore_page('news', 'Pottermore News'), mimetype='application/rss+xml')


@app.route('/pottermore/features/')
def serve_pottermore_features():
    return Response(pottermore_page('features', 'Pottermore Features'), mimetype='application/rss+xml')


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
